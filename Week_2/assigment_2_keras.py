import numpy as np
import random
import sys
import tensorflow as tf
import matplotlib.pyplot as plt
import cv2
import glob
import os
from sklearn.datasets import fetch_lfw_people
from sklearn.model_selection import train_test_split
from keras.datasets import cifar10
import h5py

class DataSet:
    def __init__(self):
        lfw_people = fetch_lfw_people(min_faces_per_person=1, color = True)
        lfw_people_sub = self.preprocess_human_faces_dataset(lfw_people,7000)
        (train_faces, test_faces) = train_test_split(lfw_people_sub, test_size = 2000, random_state = 1)
        train_faces_label = 10*np.ones((5000, 1)).astype('int')
        test_faces_label = 10*np.ones((2000, 1)).astype('int')

        cars_dataset = self.load_cars_dataset(7000)
        (train_cars, test_cars) = train_test_split(cars_dataset, test_size = 2000, random_state = 1)
        train_cars_label = 11*np.ones((5000, 1)).astype('int')
        test_cars_label = 11*np.ones((2000, 1)).astype('int')       

        licences_dataset = self.load_licences_dataset(7000)
        (train_licences, test_licences) = train_test_split(licences_dataset, test_size = 2000, random_state = 1)
        train_licences_label = 12*np.ones((5000, 1)).astype('int')
        test_licences_label = 12*np.ones((2000, 1)).astype('int')   
        
        (x_train, y_train), (x_test, y_test) = cifar10.load_data()
        
        self.train_data = self.normalize_data(np.concatenate((x_train, train_faces,train_cars,train_licences), axis = 0))
        self.test_data = self.normalize_data(np.concatenate((x_test, test_faces, test_cars,test_licences), axis = 0))
        self.train_labels = np.r_[y_train, train_faces_label, train_cars_label,train_licences_label]
        self.test_labels = np.r_[y_test, test_faces_label, test_cars_label,test_licences_label]
        
        
        self.shuffle_data()
        #idx_train = np.random.permutation(len(self.train_data))
        #self.train_data,self.train_labels = self.train_data[idx_train], self.train_labels[idx_train]
        
        idx_test = np.random.permutation(len(self.test_data))
        self.test_data,self.test_labels = self.test_data[idx_test], self.test_labels[idx_test]
        
        print('train_data shape: {0}'.format(self.train_data.shape))
        print('test_data shape: {0}'.format(self.test_data.shape))
        print('train_labels shape: {0}'.format(self.train_labels.shape))
        print('test_labels shape: {0}'.format(self.test_labels.shape))
        
        self.save_dataset_cifar12()
        
        
    def shuffle_data(self):
        idx_train = np.random.permutation(len(self.train_data))
        self.train_data,self.train_labels = self.train_data[idx_train], self.train_labels[idx_train]
    
    def _normalize(self, image):
        min_val = np.min(image)
        max_val = np.max(image)
        image_norm = (image-min_val) / (max_val-min_val)
        return image_norm

    def normalize_data(self,data):
        images_norm = []
        for image in data:
            img_norm = self._normalize(image)
            images_norm.append(img_norm)
        return np.asarray(images_norm)
      
    def preprocess_human_faces_dataset(self, dataSet, size_new):
        random.seed(64)
        random.shuffle(dataSet.images)
        dataSet_sub = dataSet.images[:size_new]
        images_res = []
        for image in dataSet_sub:
            img_res = cv2.resize(image, (32, 32))
            images_res.append(img_res)
        return np.asarray(images_res)
    
    def load_cars_dataset(self,size_new):
        link_names = []
        for name in glob.glob('C:/Users/huynh/OneDrive/Desktop/Workspace/MachineLearning_DeepLearning/PracticalDeepLearning_CV/Week_2/Assignment2/cars_train/*'):
            link_names.append(name)

        random.seed(64)
        random.shuffle(link_names)
        link_names = link_names[:size_new]

        images_res = []
        for link in link_names:
            image = cv2.imread(link)
            img_res = self.resize_keep_aspect_ratio(image, (32, 32))
            images_res.append(img_res)
        return np.asarray(images_res)
    

    def load_licences_dataset(self,size_new):
        link_names = []
        for name in glob.glob('C:/Users/huynh/OneDrive/Desktop/Workspace/MachineLearning_DeepLearning/PracticalDeepLearning_CV/Week_2/Assignment2/2017-IWT4S-CarsReId_LP-dataset/*/*'):
            link_names.append(name)

        random.seed(64)
        random.shuffle(link_names)
        link_names = link_names[:size_new]

        images_res = []
        for link in link_names:
            image = cv2.imread(link)
            img_res = self.resize_keep_aspect_ratio(image, (32, 32))
            images_res.append(img_res)
        return np.asarray(images_res)


    def  resize_keep_aspect_ratio(self,img, size, padColor=0):
        h, w = img.shape[:2]
        sh, sw = size

        # interpolation method
        if h > sh or w > sw: # shrinking image
            interp = cv2.INTER_AREA

        else: # stretching image
            interp = cv2.INTER_CUBIC

        # aspect ratio of image
        aspect = float(w)/h 
        saspect = float(sw)/sh

        if (saspect > aspect) or ((saspect == 1) and (aspect <= 1)):  # new horizontal image
            new_h = sh
            new_w = np.round(new_h * aspect).astype(int)
            pad_horz = float(sw - new_w) / 2
            pad_left, pad_right = np.floor(pad_horz).astype(int), np.ceil(pad_horz).astype(int)
            pad_top, pad_bot = 0, 0

        elif (saspect < aspect) or ((saspect == 1) and (aspect >= 1)):  # new vertical image
            new_w = sw
            new_h = np.round(float(new_w) / aspect).astype(int)
            pad_vert = float(sh - new_h) / 2
            pad_top, pad_bot = np.floor(pad_vert).astype(int), np.ceil(pad_vert).astype(int)
            pad_left, pad_right = 0, 0

        # set pad color
        if len(img.shape) is 3 and not isinstance(padColor, (list, tuple, np.ndarray)): # color image but only one color provided
            padColor = [padColor]*3

        # scale and pad
        scaled_img = cv2.resize(img, (new_w, new_h), interpolation=interp)
        scaled_img = cv2.copyMakeBorder(scaled_img, pad_top, pad_bot, pad_left, pad_right, borderType=cv2.BORDER_CONSTANT, value=padColor)

        return scaled_img

    
    def get_train_set_size(self):
        return self.train_data.shape[0]

    def get_test_set_size(self):
        return self.test_data.shape[0]

    def to_one_hot(self, X):
        one_hot = np.zeros((len(X), 11))
        for i in range(len(X)):
            np.put(one_hot[i, :], X[i], 1)

        return one_hot
    def save_dataset_cifar12(self):
        with h5py.File('.\Cifar13.hdf5', 'w') as hf:
          hf.create_dataset("X_train", data=self.train_data)
          hf.create_dataset("X_test", data=self.test_data)
          hf.create_dataset("Y_train", data=self.train_labels)
          hf.create_dataset("Y_test", data=self.test_labels)
      
    # Helper function to plot some images in the dataset
    def plot_cifar13(self,class_name=None):
      mapping = {'airplane': 0, 'automobile': 1, 'bird': 2, 'cat': 3, 'deer': 4,
                'dog': 5, 'fog': 6, 'horse': 7, 'ship': 8, 'truck': 9, 'human':10, 'car':11, 'license':12}

      if class_name == None:
        fig, ax = plt.subplots(13, 10, sharey=True, figsize=(12,10))
        for j in range(13):
          class_index = (self.train_labels == j)[:,0] #get all values in column 0 (from all rows)
          class_imgs =  self.train_data[class_index,:,:][np.random.choice(5000, 10),:,:]
          for i, img in enumerate(class_imgs):
            ax[j,i].imshow(img)
            ax[j,i].grid('off')
            ax[j,i].set_xticks([])
            ax[j,i].set_yticks([])
            if i == 0:
              ax[j,i].set_ylabel(list(mapping.keys())[j])
      else:
        class_index = (self.train_labels == mapping[class_name])[:,0]
        class_imgs = self.train_data[class_index,:,:][np.random.choice(5000, 10),:,:]
        fig, ax = plt.subplots(1, 10, sharey=True, figsize=(22,2))
        for i, img in enumerate(class_imgs):
          ax[i].imshow(img)
          ax[i].grid('off')
          ax[i].set_xticks([])
          ax[i].set_yticks([])
          if i == 0:
            ax[i].set_ylabel(class_name)
        

dataset = DataSet()

# dataset.plot_cifar12('cat')

print("read data\n")
data = h5py.File('Cifar12.hdf5','r')  # Get features and labels
print(list(data.keys()))

train_human_x = data["X_train"]
test_human_x = data["X_test"]
train_human_y = data["Y_train"]
test_human_y = data["Y_test"]

print(train_human_y[1])
# print('Shape of train_human_x = ' + str(train_human_x.shape))
# print('Shape of train_human_y = ' + str(train_human_y.shape))
# print('Shape of test_human_x = ' + str(test_human_x.shape))
# print('Shape of test_human_y = ' + str(test_human_y.shape))

#plot_cifar11('cat')

from matplotlib import pyplot
for i in range(0, 9):
	pyplot.subplot(330 + 1 + i)
	pyplot.imshow(test_human_x[6000+i])
# show the plot
pyplot.show()